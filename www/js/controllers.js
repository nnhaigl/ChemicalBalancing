angular.module('starter.controllers', [])

.controller('DashCtrl', function($scope, $ionicPopup) {
  $scope.phuongtrinh={
    input:"",
    result:"dsds"
  };
  $scope.refresh=function(){
    $scope.phuongtrinh.input="";
  }
  $scope.Balance=function(){
    var MyDiv2 = document.getElementById('result');
      MyDiv2.innerHTML="";
    try{
      $scope.phuongtrinh.result = balance($scope.phuongtrinh.input);
      
      MyDiv2.appendChild($scope.phuongtrinh.result);
    }
    catch(err)
    {
      MyDiv2.appendChild("");
      var alertPopup = $ionicPopup.alert({
        title: 'Thông báo',
        template: 'Phương trình không thể cân bằng. Vui lòng kiểm tra lại phương trình'
      });
    }
  }
})

.controller('ChatsCtrl', function($scope, Chats) {
  // With the new view caching in Ionic, Controllers are only called
  // when they are recreated or on app start, instead of every page change.
  // To listen for when this page is active (for example, to refresh data),
  // listen for the $ionicView.enter event:
  //
  //$scope.$on('$ionicView.enter', function(e) {
  //});

  $scope.chats = Chats.all();
  $scope.remove = function(chat) {
    Chats.remove(chat);
  };
})

.controller('ChatDetailCtrl', function($scope, $stateParams, Chats) {
  $scope.chat = Chats.get($stateParams.chatId);
})

.controller('AccountCtrl', function($scope) {
  $scope.settings = {
    enableFriends: true
  };
});
